
# SHARING IS CARING   
=======================

This folder contains the documentation produced at the occasion of JODI's performance THIS PAGE CONTAINS..., held in the Teijin Auditorium at the Stedelijk Museum on the 1st October 2015. The dissemination of this folder relies on the collaboration of its possessors to maintain its availability.

This folder is aimed for distribution of the documentation produced by their different authors, showing a variety of points of views. The files are shared for information, preservation and rediscovery of the performance.

The documents are free to be reused in various ways, at the only condition that such initiatives are notified to and shared with their authors.

This project was initiated by Annet Dekker in association with LIMA, an Amsterdam based platform for preserving, distributing and researching media art. This was made possible with the support of the Stedelijk museum.

For more information :
http://stedelijk.nl/en/calendar/performances/79887
http://www.li-ma.nl



# List of all documents
=======================

1. [ 29MinutesofJODI.mp4 ] by Thomas Waalskar
....... Video record of the event from the audience
....... Duration: 29'01''
....... Size: 1,15 GB
....... Resolution: 1920 × 1080
....... Codecs: H.264, AAC


2. [ 20151001_JODI-Colour-Coded ] by Molly Bower and Nina van Doren
....... 1_SPACE_TIME.png 
....... 2_AUDIENCE.png
....... 3_TECHNICAL_DESCRIPTION.png
....... 4_ASSOCIATIONS.pdf
....... 4_ASSOCIATIONS.png
....... 5_PERFORMANCE_DESCRIPTION.png
....... 6A_ARTISTS.png
....... 6B_INTERVIEW_RESPONSES.png
....... 20151001_JODI-Colour-Coded_README.pdf

3. [ Battich_document-JODI_This_Page_Contains.pdf ] 
...[ Publication--JODI_This_Page_Contains_1-5.jpg ] by Lucas Battich
....... Written account of the event, combining presonal impressions with objective script logs.


4. [ docu--Sted ] by JODI
....... 1 folder containing:
....... 51 screenshots of the performance
....... 4 text files
....... 1 email screenshot regarding event ticket reservation.
....... formats : png, txt


5. [ emailConvs ] by all
....... Format: pdf
.............. exchanges between the participants throughout the project


6. [ Jodi_final_v4.mp4 ] by Michaela Lakova
....... Video reinterpretation
....... Duration: 1'13''
....... Size: 300.3 MB
....... Resolution: 1920 × 1080
....... Codecs: H.264, AAC
....... Used Materials for the sound and the video :
.............. timelapse from the actual performance ( 20min - 25sec)
.............. sound records from the actual performance 
.............. screenshots from JODI screen (provided by the artists)
.............. text files provided by JODI ( speak-wget command.txt & speak-erco.txt)
.............. Stedelijk logo(s) --Internet 
.............. LIMA logo (homemade)


7. [ JODIbyhelia.pdf ] by Hélia Marçal 
....... Written personal impressions of the event


8. [ JODI@STDLJK - This folder contains ] by Julie Boschat
....... JODI@STDLJK - This folder contains.torrent
....... JODI@STDLJK - This folder contains.zip
....... JODI@STDLJK - This folder contains.git
....... Size: 1.49 GB
..............  a folder compiling the documentation of JODI's performance THIS PAGE CONTAINS and strategies for its dissemination



# Biographies and contact details
=================================


1. Lucas Battich (AR)

Lucas Battich is a multi-disciplinary artist, born in San Miguel de Tucumán, Argentina. He studied at Stevenson College Edinburgh, Duncan of Jordanstone College of Art and Design, University of Dundee and the Piet Zwart Institute, Rotterdam.

http://lucasbattich.com
lucas.battich@gmail.com

.....................................................

2. Julie Boschat (FR) 

Julie Boschat tries to find tangibility in computer networks. She is also completing a Master program in Media Design and Communication MA at Piet Zwart Institute. 

jboschatthorez@gmail.com

.....................................................

3. Molly Bower (US) 

Molly Bower is a moving image and media art conservator completing a master's degree in Preservation and Presentation of the Moving Image, and she is currently researching intern at LIMA. Her research focuses on media art preservation and the politics of archiving  populist media formats. She holds a bachelor's degree in Cultural Studies from McGill University in Montreal. 

mollybower@li-ma.nl

.....................................................

4. Annet Dekker (NL)

Annet Dekker is an independent researcher and curator. She is currently  Researcher Digital Preservation at TATE, London, Post-doc Research  Fellow at London South Bank University / The Photographers Gallery, and  core tutor at Piet Zwart Institute, Rotterdam (Master Media Design and  Communication, Networked Media and Lens-Based Media). Previously she  worked as Web curator for SKOR (Foundation for Art and Public Domain,  2010/12), was programme manager at Virtueel Platform (2008/10), and head  of exhibitions, education and artists-in-residence at the Netherlands  Media Art Institute (1999/2008). From 2008-14 she wrote her Ph.D. Enabling  the Future, or How to Survive FOREVER. A study of networks, processes  and ambiguity in net art and the need for an expanded practice of  conservation, at the Centre for Cultural Studies, Goldsmiths University, London.

http://aaaan.net/
annet@aaaan.net 

.....................................................

5. JODI (BE/NL)

Joan  Heemskerk and Dirk Paesmans together form JODI, or jodi.org, an artistic duo living and working in Dordrecht (NL). JODI was known for  pioneering internet art during the 1990s, and since then has acquired an  important position in digital avant-garde art both nationally and  internationally. Their work developed from photography and video to  websites, software and computer technology. In 2008, the Stedelijk  presented their work Untitled Game in the exhibition Deep Screen Art in  Digital Culture, consisting of a game based on the code of the  well-known game Quake. Recently they were awarded the Prix NetArt 2014  for their web works and their installation My%Desktop (2002) was  acquired by the MoMA in 2015.

http://wwwwwwwww.jodi.org/
jodi@jodi.org

.....................................................

6. Michaela Lakova (BG/ NL)

Michaela Lakova is a visual artist and researcher based in Rotterdam.Subjects of interest are errors, systems malfunction and the inevitable generation of data traces and its problematic resistance to deletion. Currently she is investigating into digital forensics. She completed a Master program in Media Design and Communication MA at Piet Zwart Institute and she holds a BA in Stage and Screen design from National Academy for Theater and Film Arts, Sofia. Michaela occasionally works as a freelancer videographer / photographer within the cultural sector. 

http://www.mlakova.org
http://video.mlakova.info
contact@mlakova.org

.....................................................

7. Hélia Pereira Marçal (PT)

Hélia Marçal (b. 1988) received her Master degree in Conservation and Restoration from Universidade Nova de Lisboa in 2012 with a dissertation entitled "Embracing transience and subjectivity in the conservation of complex contemporary artworks: contributions from ethnographic and psychological paradigms". While enrolled on her bachelor's degree, she has been assigned two research scholarships to study archival collections (2009) and in transparent electronics' investigation (2010). She is now a PhD Fellow in the same University with a project funded by Fundação para a Ciência e Tecnologia. Her PhD project verses in the preservation of performance-based artworks focusing on a critical analysis of documentation strategies and in the study of decision-making in the conservation of these works. Since the beginning of 2015 she has also been Assistant Coordinator from Theory & History Working Group in ICOM-CC (International Council of Museums - Committee for Conservation). 

http://sites.fct.unl.pt/doutoramento-conservacao-restauro-patrimonio/pages/helia-marcal-0
hp.marcal@gmail.com

.....................................................

8. Nina van Doren (NL)

Nina van Doren is a researcher and documentalist for LIMA. She holds a BA in Art History from the University of Amsterdam and is currently completing an MA in Comparative Arts and Media Studies at the VU University Amsterdam. Her main research focusses on the preservation and institutionalisation of software-based art. 

nlv.doren@gmail.com

.....................................................

9. Thomas Walskaar (NO) 

Thomas Walskaar is a  multi-disciplinary Graphic Designer based in Rotterdam currently completing a Master program in Media Design and Communication MA at Piet Zwart Institute. His main research focus is in the topic of archiving in the digital age. He holds a BA Hons Graphic Design from Ravensbourne in London.

www.walska.com
thomas@walska.com



# Links to index
================

If you feel like hosting a folder yourself, sharing your own torrent version or hosting a git repository, please drop an email to jboschatthorezatgmail.com and the link will be added to the index available at :  

http://pzwart1.wdka.hro.nl/~jules/20151001_JODIatSTDLJK/
http://s192400.gridserver.com/20151001_JODIatSTDLJK/
http://here-you-are.com/20151001_JODIatSTDLJK/


     ."".    ."",
     |  |   /  /
     |  |  /  /
     |  | /  /
     |  |/  ;-._ 
     }  ` _/  / ;
     |  /` ) /  /
     | /  /_/\_/\
     |/  /      |
     (  ' \ '-  |
      \    `.  /
       |      |
       |      |

